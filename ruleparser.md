
```fowltalk

Object subclass: #RuleParser.

RuleParser subclass: #CharParser variables: #(#match).
RuleParser subclass: #StrParser variables: #(#match).

RuleParser
  subclass: #BinaryRuleParser
  variables: #(#lhs. #rhs).

RuleParser subclass: #ContainerRule variables: #(#rules).
def ContainerRule rules: rs
[ rules = rs ].
def MetaContainerRule rules: rs
[|o|
  o = self new.
  o rules: rs
  o
].


ContainerRule subclass: #ConjRule.
def ConjRule match: state
[|s1 res|
  s1 = state save.
  res = #().
  rules do: [|:rule x |
    x = rule match: state.
    x or: [ state restore: s1. ^nil ].
    (x == true) or: [
      res = res insert: x at: (res size + 1)
    ]
  ].
  res isEmpty ifTrue: [^true].
  res size == 1 ifTrue: [^res at: 1].
  res
].

ContainerRule subclass: #DisjRule.
def DisjRule match: state
[|s1|
  s1 = state save.
  rules do: [|:rule res|
    res = rule match: state.
    res ifTrue: [ ^res ].
    state restore: s1.
  ].
  nil
].


def RuleParser & rule
[
  ConjRule rules: (#self. rule)
].
def RuleParser | rule
[
  DisjRule rules: #(self. rule)
].

def ConjRule & rule
[
  ConjRule rules: (rules add: rule)
].
def DisjRule | rule
[
  DisjRule rules: (rules add: rule)
].

Parser subclass: #InputState.
def InputState save
[
  InputState new input: @input pos: @pos
].
def InputState restore: state
[
  input = state input.
  pos = state pos.
  self
].

def CharParser match: state
[|c|
  c = state char.
  c == @match
    ifTrue: [state consumeChar. ^c]
    else: [^nil]
].
def StrParser match: s
[
  (s skipString: @match)
    ifTrue: [^ @match]
    else: [^ nil]
].

RuleParser subclass: #CharRangeRule variables: #(#begin. #end).



```
