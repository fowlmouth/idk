#pragma once

template<
class T>
struct region
{
    static uintptr_t base;
};

template<
    class T,
    class OffsetType = uintptr_t,
    class Region = region<T> >
class abs_ptr
{
    OffsetType offset;
public:
    using self_t = abs_ptr<T,OffsetType,Region>;
    using type = T;
    using offset_t = OffsetType;
    using region_t = Region;


    const offset_t& get_offset() const
    {
        return offset;
    }

    inline abs_ptr(): offset(self_t::null)
    {
    }
    inline explicit abs_ptr(const T* ptr)
    {
        set(ptr);
    }
    inline explicit abs_ptr(OffsetType offs): offset(offs)
    {
    }
    abs_ptr(const self_t& other): offset(other.offset)
    {
    }

    inline bool operator== (const self_t& other) const
    {
        return offset == other.offset;
    }

    inline T* get() const
    {
        return offset == self_t::null
               ? nullptr
               : reinterpret_cast<T*>(Region::base + offset);
    }
    inline T* operator->() const
    {
        return get();
    }

    inline void set(const T* ptr)
    {
        offset = ptr
                 ? reinterpret_cast<uintptr_t>(ptr) - Region::base
                 : self_t::null;
    }

    inline self_t& operator= (const T* ptr)
    {
        set(ptr);
        return *this;
    }
    inline self_t& operator= (const self_t& other)
    {
        offset = other.offset;
        return *this;
    }

    static uintptr_t base()
    {
        return Region::base;
    }
    inline uintptr_t get_base()
    {
        return self_t::base();
    }

    static const uintptr_t null = 1;
    inline void reset()
    {
        offset = self_t::null;
    }
    inline bool is_null() const
    {
        return offset == self_t::null;
    }
    inline explicit operator bool() const
    {
        return !is_null();
    }
    inline explicit operator offset_t()
    {
        return get_offset();
    }

    inline void set_offset(OffsetType offs)
    {
        offset = offs;
    }

    static const self_t start ()
    {
        self_t ptr;
        ptr.set_offset(0);
        return ptr;
    }




    template<typename U>
    self_t operator<< (U shift) const
    {
        self_t new_ptr(offset << shift);
        //new_ptr.set_offset(offset << shift);
        return new_ptr;
    }

    template<typename U>
    self_t operator>> (U shift) const
    {
        self_t new_ptr(offset >> shift);
        return new_ptr;
    }

    /*
     template<typename S>
      friend self_t operator<< (self_t left, S right);*/
};

/*
template<typename Int, typename... Ts>
abs_ptr<Ts...> operator << (abs_ptr<Ts...> left, Int right)
{
  abs_ptr<Ts...> newp(left.get_offset() << right);
  return newp;
}*/

