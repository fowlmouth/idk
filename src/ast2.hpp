
#pragma once
#include <vector>
#include <string>
#include <cstring>
#include <algorithm>
#include <bitset>

class node
{
public:
  enum kind
  {
    lit_nil, lit_true, lit_false,
    lit_num,
    lit_str, lit_ident, lit_sym,
    msg, array
  };
protected:
  kind k;
  union
  {
    INT_TYPE num;
    const char* cstr;
    node** children;
  };
  
  node(const kind& k): k(k), num(0)
  {
  }
  
public:
  ~node()
  {
    if(k >= lit_str || k <= lit_sym)
    {
      delete cstr;
      return;
    }
    if(k > lit_sym)
    {
      for(auto n = children[0]; n; ++n)
	delete n;
      delete children;
      return;
    }
  }
  
  kind get_kind()
  {
    return k;
  }
  inline const bool is_str() const
  {
    return k >= lit_str && k <= lit_sym;
  }
  inline const bool is_container() const
  {
    return k > lit_sym;
  }
  inline const bool is_num() const
  {
    return k == lit_num;
  }
  const char* get_str()
  {
    assert(is_str());
    return cstr;
  }
  node** get_children()
  {
    assert(is_container());
    return children;
  }
  INT_TYPE get_num()
  {
    assert(is_num());
    return num;
  }
  
  
  
  static node* string (const std::string& str, const kind& kind = lit_str)
  {
    auto c = new char[str.size()+1];
    std::memcpy(c, str.c_str(), str.size());
    auto r = new node(kind);
    r->cstr = c;
    return r;
  }
  static node* number (const INT_TYPE& num, const kind& kind = lit_num)
  {
    auto r = new node(kind);
    r->num = num;
    return r;
  }
  
  static node* singleton (const kind& kind)
  {
    assert(kind <= lit_false);
    return new node(kind);
  }
  static node* nil_lit() { return singleton(lit_nil); }
  static node* true_lit(){ return singleton(lit_true);}
  static node* false_lit(){return singleton(lit_false);}
  
};



#include "grammar.hpp"
glossolalia::rule<node*>* create_parser ()
{
  using gr = glossolalia::grammar<node*>;
  auto digit = new gr::chs();
  auto digits = new gr::repeat(digit, 1);
  
  
  
}



/*

class astnode
{
public:
  virtual ~astnode()
  {
  }
};

template<class T>
class literal: public astnode
{
  const T value;
public:
  literal(const T& val): value(val)
  {
  }
  virtual ~literal()
  {
  }
  
};

class int_lit: public literal<INT_TYPE>
{
  using literal::literal;
};

class message: public astnode
{
  astnode *recv;
  std::vector<astnode*> args;
  std::string sel;
  
public:
  message(astnode* recv, const std::vector<astnode*> &args, const std::string &sel):
    recv(recv), args(args), sel(sel)
  {
  }
public:
  virtual ~message()
  {
    delete recv;
    for(const auto& it: args)
      delete it;
  }
  
};

class nil_lit: public astnode
{
public:
  ~nil_lit(){}
};

class ident: public literal<std::string>
{
  using literal::literal;
};
class symbol: public ident
{
  using ident::ident;
};
*/


struct context_builder
{
  context_builder *parent, *method;
  std::vector<std::string> locals;
  
  context_builder(): parent(nullptr), locals()
  {}
  
  
  struct find_result
  {
    std::size_t parents, index;
    
  };
  bool find (const std::string& s, find_result& out, std::size_t offs)
  {
    const auto& fi = std::find(locals.begin(), locals.end(), s);
    if(fi == locals.end())
      return parent
	? parent->find(s, out, offs+1)
	: false;
	
    out.parents = offs;
    out.index = fi - locals.begin();
    return true;
  }
  
  bool find (const std::string& s, find_result& out)
  {
    return find(s, out, 0);
  }
  
  
};



using glossolalia::grammar;
using glossolalia::input_state;
using glossolalia::add_range;
using namespace glossolalia;

void test_rules()
{
  using gr = grammar<node*>;
  gr::chs::bitset_t bs;
  
  add_range(bs, 'a', 'z');
  add_range(bs, 'A', 'Z');
  add_range(bs, '0', '9');
  bs.set('_', true);
  auto ident_char = new gr::chs(bs);
  auto multi = new gr::seq({ident_char, ident_char, ident_char});
  auto multi2 = new gr::capture(multi, [&](const std::string &name)->node*{
    return node::string(name, node::lit_ident);
  });
  auto is = input_state("ab_");
  auto x = multi2->match(is);
  assert(x);
  assert(x.get_state() == nodes);
  
  auto nxx = new gr::repeat(ident_char, 0, -1);
  is.pos = 0;
  x = nxx->match(is);
  assert(x);
  SHOWSRC(x.get_range().first);
  SHOWSRC(x.get_range().second);
  
  //auto ident = new capture<node>()
}

void testss()
{
  auto r = new chs<node>({'a','b'});
  auto is = input_state("aba");
  {
    auto res = r->match(is);
    SHOWSRC(res.get_state());
    if(res.get_state() == 1)
    {
      SHOWSRC(res.get_range().first);
      SHOWSRC(res.get_range().second);
    }
  }
  
  is.pos = 0;
  {
    auto r2 = new seq<node>({r,r});
    auto res = r2->match(is);
    SHOWSRC(res.get_state());
    SHOWSRC(res.get_range().first);
    SHOWSRC(res.get_range().second);
  }
  
  is.pos = 0;
  {
    auto r3 = new seq<node>({r, new chs<node>({'c'})});
    auto res = r3->match(is);
    SHOWSRC(res.get_state());
    auto is2 = input_state("bc");
    SHOWSRC(r3->match(is2).get_state());
  }
}







node* parse_expr (input_state& s);


node* parse_int (input_state& s)
{
  if(!isdigit(s.current())) return nullptr;
  
  INT_TYPE r = 0;
  do{
    r = (r * 10) + (s.eat() - '0');
  } while(isdigit(s.current()));
  return node::number(r);
}
node* parse_sym (input_state& s)
{
  if(s.current() == '#')
  {
    s.pos++;
    std::string kwd;
    while(isalnum(s.current()) || s.current() == ':')
      kwd.push_back(s.eat());
    return node::string(kwd, node::kind::lit_sym);
  }
  return nullptr;
}

node* parse_special (input_state& s)
{
  if(s.compare("nil") == 0 && !isalnum(s.next(3)))
    return node::nil_lit();
  if(s.compare("true") == 0 && !isalnum(s.next(4)))
    return node::true_lit();
  if(s.compare("false") == 0 && !isalnum(s.next(5)))
    return node::false_lit();
  
  return nullptr;
}
/*
if(isalpha(is.current()))
{
  std::string kwd;
  while(isalnum(is.current()) || is.current() == ':')
    kwd.push_back(is.eat());
  return new ident(kwd);
}
return nullptr;*/

node* parse_terminal (input_state& s)
{
  auto res = parse_int(s);
  if(res) return res;
  res = parse_sym(s);
  if(res) return res;
  res = parse_special(s);
  if(res) return res;
  
  
  return nullptr;
  
}

bool parse_ident (input_state& is, std::string& out)
{
  if(isalpha(is.current()))
  {
    while(isalnum(is.current()))
    {
      out.push_back(is.eat());
    }
    return true;
  }
  return false;
}

void skip_whitespace(input_state& is)
{
  auto c = is.current();
  while(c == ' ' || c == '\t' || c == '\n' || c == '\r')
  { is.eat(); c = is.current(); }
}
void skip_spaces (input_state& is)
{
  while(is.current() == ' ' || is.current() == '\t')
    is.eat();
}

bool parse_operator (input_state& is, std::string& out)
{
  auto c = is.current();
  auto res = false;
  while(c == '+' || c == '-' || c == '*' || c == '/')
  {
    is.eat();
    out.push_back(c);
    c = is.current();
    res = true;
  }
  return res;
}


node* parse_expr (input_state& is)
{
  auto st = is.save_position();
  //auto recv = parse_binary(is);
  
  return nullptr;
}

node* parse_block (input_state& is)
{
  if(is.current() != '[') return nullptr;
  is.eat();
  
  skip_spaces(is);
  std::vector<std::string> args;
  if(is.current() == '|')
  {
    is.eat();
    skip_spaces(is);
    std::string id;
    while(parse_ident(is, id))
    {
      args.push_back(id);
      id.clear();
      skip_spaces(is);
    }
    assert(is.current() == '|');
    is.eat();
    skip_whitespace(is);
  }
  std::vector<node*> stmts;
  while(is.current() != '\0')
  {
    node* s = parse_expr(is);
    if(!s) break;
    stmts.push_back(s);
  }
  SHOWSRC(stmts.size());
  SHOWSRC(args.size());
  
  return nullptr;
}

bool parse_method_header (input_state& is, std::string& selector, std::vector<std::string>& args)
{
  if(parse_operator(is, selector))
  {
    skip_spaces(is);
    std::string arg;
    assert(parse_ident(is, arg));
    args.push_back(arg);
    return true;
  }
  
  auto iters = 0;
  while(true)
  {
    if(parse_ident(is, selector))
    {
      if(is.current() == ':')
      {
	selector.push_back(is.eat());
	skip_spaces(is);
	std::string argname;
	assert(parse_ident(is, argname));
	skip_spaces(is);
	args.push_back(argname);
      }
      else
      {
	assert(iters == 0);
	assert(args.size() == 0);
	return true;
      }
    }
    else
    {
      return iters > 0;
    }
    iters++;
  }
}

oop_handle parse_method (const oop_handle& cls, const std::string& str)
{
  input_state s(str);
  std::vector<std::string> args;
  std::string selector;
  assert(parse_method_header(s, selector, args));
  std::cout << selector << std::endl;
  skip_whitespace(s);
  node* n = parse_block(s);
  
  return oop_handle();
  
}
