#pragma once

namespace oop_class_slots {
    enum{
        name = 1,
        superclass,
        methods,
        instance_size,
        variables,

        slots = variables
    };
}

namespace oop_method_slots{
    enum{
        name = 1,
        bytecodes, //the vm instructions for this method
        immediates, //constant values used in bytecodes
        locals, //count of arguments+locals
        code, //string or ast node containing code
        klass, //class that owns this method

        slots = klass
    };
}

namespace context {
    enum{
        method = 1,
        stack, //2
        caller, //3
        ip, //4
        sp, //5
        locals, //6

        slots = locals
    };
}

namespace block_context {
    enum{
        method = context::method,
        stack = context::stack,
        caller = context::caller,
        ip = context::ip,
        sp = context::sp,
        locals = context::locals, //6
        parent, //7
        low_ip, //8

        slots = low_ip
    };
}

namespace process {
    enum{
        context = 1,
        status,
        result,

        slots = result
    };
}

namespace image {
    enum{
        globals = 1,
        int_class,
        nil_class,
        true_class,
        false_class,
        sym_class,

        slots = sym_class
    };
}

namespace message {
    enum{
        recv = 1,
        name,
        args,

        slots = args
    };
}
