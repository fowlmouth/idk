#pragma once
#include <nanogui/screen.h>
#include <nanogui/window.h>
#include <nanogui/layout.h>
#include <nanogui/label.h>
#include <nanogui/checkbox.h>
#include <nanogui/button.h>
#include <nanogui/toolbutton.h>
#include <nanogui/popupbutton.h>
#include <nanogui/combobox.h>
#include <nanogui/progressbar.h>
#include <nanogui/entypo.h>
#include <nanogui/messagedialog.h>
#include <nanogui/textbox.h>
#include <nanogui/slider.h>
#include <nanogui/imagepanel.h>
#include <nanogui/imageview.h>
#include <nanogui/vscrollpanel.h>
#include <nanogui/colorwheel.h>
#include <nanogui/graph.h>
#include <nanogui/glutil.h>
#include <iostream>


struct Visualizer: public nanogui::Screen
{
	oop_space* space;

	Visualizer(oop_space* space)
	: nanogui::Screen(Eigen::Vector2i(1024, 800), "test"),
	  space(space)
	{
		using namespace nanogui;
		auto w = new Window(this, "globals");
		w->setPosition(Vector2i(15,15));
		//w->setLayout();

        auto p = new VScrollPanel(w);
        p->setLayout(new BoxLayout(Orientation::Vertical) );

		const oop_handle& globals = space->globals();
        const oop_handle& keys = globals.get_obj()->get_field(1);
		for(int i = 1; i < keys.get_obj()->num_slots(); ++i)
		{
			const oop_handle& key = keys.get_obj()->get_field(i);
			auto cs = key.get_obj()->cstr();
			Print(cs);
			auto b = new Button(p, cs);
		}

        performLayout(mNVGContext);

        /* All NanoGUI widgets are initialized at this point. Now
           create an OpenGL shader to draw the main window contents.
           NanoGUI comes with a simple Eigen-based wrapper around OpenGL 3,
           which eliminates most of the tedious and error-prone shader and
           buffer object management.
        */

        mShader.init(
            /* An identifying name */
            "a_simple_shader",

            /* Vertex shader */
            "#version 330\n"
            "uniform mat4 modelViewProj;\n"
            "in vec3 position;\n"
            "void main() {\n"
            "    gl_Position = modelViewProj * vec4(position, 1.0);\n"
            "}",

            /* Fragment shader */
            "#version 330\n"
            "out vec4 color;\n"
            "uniform float intensity;\n"
            "void main() {\n"
            "    color = vec4(vec3(intensity), 1.0);\n"
            "}"
        );

        MatrixXu indices(3, 2); /* Draw 2 triangles */
        indices.col(0) << 0, 1, 2;
        indices.col(1) << 2, 3, 0;

        MatrixXf positions(3, 4);
        positions.col(0) << -1, -1, 0;
        positions.col(1) <<  1, -1, 0;
        positions.col(2) <<  1,  1, 0;
        positions.col(3) << -1,  1, 0;

        mShader.bind();
        mShader.uploadIndices(indices);
        mShader.uploadAttrib("position", positions);
        mShader.setUniform("intensity", 0.5f);
    }

    ~Visualizer() {
        mShader.free();
    }

    virtual bool keyboardEvent(int key, int scancode, int action, int modifiers) {
        if (Screen::keyboardEvent(key, scancode, action, modifiers))
            return true;
        if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS) {
            setVisible(false);
            return true;
        }
        return false;
    }

    virtual void draw(NVGcontext *ctx) {
        /* Animate the scrollbar */
        //mProgress->setValue(std::fmod((float) glfwGetTime() / 10, 1.0f));

        /* Draw the user interface */
        Screen::draw(ctx);
    }

    virtual void drawContents() {
        using namespace nanogui;

        /* Draw the window contents using OpenGL */
        //mShader.bind();

        // Matrix4f mvp;
        // mvp.setIdentity();
        // mvp.topLeftCorner<3,3>() = Matrix3f(Eigen::AngleAxisf((float) glfwGetTime(),  Vector3f::UnitZ())) * 0.25f;

        // mvp.row(0) *= (float) mSize.y() / (float) mSize.x();

        // mShader.setUniform("modelViewProj", mvp);

        // /* Draw 2 triangles starting at index 0 */
        // mShader.drawIndexed(GL_TRIANGLES, 0, 2);
    }
private:
    nanogui::ProgressBar *mProgress;
    nanogui::GLShader mShader;

};

template<typename Screen = Visualizer, typename... Args>
int main_handler(Args&&... args) {
    try {
        nanogui::init();
        {
            nanogui::ref<Screen> app = new Screen(std::forward<Args>(args)...);
            app->drawAll();
            app->setVisible(true);
            nanogui::mainloop();
        }

        nanogui::shutdown();
    } catch (const std::runtime_error &e) {
        std::string error_msg = std::string("Caught a fatal error: ") + std::string(e.what());
        #if defined(_WIN32)
            MessageBoxA(nullptr, error_msg.c_str(), NULL, MB_ICONERROR | MB_OK);
        #else
            std::cerr << error_msg << endl;
        #endif
        return -1;
    }

    return 0;
}
