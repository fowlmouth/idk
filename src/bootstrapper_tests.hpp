
ast::expr parse_expr(const std::string& def)
{
    g2<std::string::const_iterator> grammar;
    cout << "parsing " << def << endl;

    std::string::const_iterator start=def.begin(), end=def.end();
    ast::expr res;
    assert(boost::spirit::qi::phrase_parse(start, end, grammar, boost::spirit::ascii::space, res));
    return res;
}

// template<
//     template<typename...> class Grammar = g2,
//     typename It = std::string::const_iterator,
//     typename Skip = qi::ascii::space_type
// >
// class toplevel_stmts: public qi::grammar<It, std::vector<ast::expr>(), Skip>
// {
//     qi::rule<It, std::vector<ast::expr>(), Skip> start;
//     Grammar<It, Skip> sub;
// public:
//     toplevel_stmts(): toplevel_stmts::base_type(start)
//     {
//         start = (sub % qi::lit('.')) >> -qi::lit('.') >> qi::eoi;
//     }
// };


void show_bytecode (vm::codegen& cg)
{
    for(std::vector<unsigned char>::const_iterator start= cg.get_bytes().begin(), end= cg.get_bytes().end();
        start != end;)
    {
        vm::opcode op = (vm::opcode) *start++;
        std::cout << vm::opcode_to_str(op);
        return;
    }
}





bool read_file (const char* file, std::string& out)
{
    std::ifstream f(file);
    out.clear();
    f.seekg(0, std::ios::end);
    out.reserve(f.tellg());
    f.seekg(0, std::ios::beg);
    out.assign(std::istreambuf_iterator<char>(f), std::istreambuf_iterator<char>());
    return true;
}

oop_space* create_bootstrap_space(unsigned size, const std::string& file)
{
    auto sp = oop_space::create(size);
    auto bs = new interp::bootstrapper(sp);
    bs->strapboots(file);

    return sp;
}

oop_space* test_vm_bootstrap()
{

    auto sp = oop_space::create(1 << 16);
    auto bs = new interp::bootstrapper(sp);
    bs->strapboots("bootstrap");
    bs->t1();

    auto o1 = sp->alloc(8,false);
    SHOWSRC(o1->get_size());
    {
        auto p = oop_handle(o1);

    }
    SHOWSRC("----");
    //bs->part2();
    bs->t1();
    //bs->test_classes();

    //testss();

    //const auto& ocls = bs->find_global("Object");
    //assert(ocls);
    std::string input;
    read_file("bootstrap", input);
    bs->run_code(input);

    Print("** Creating globals dictionary...");
    oop_handle sc = bs->find_global("Symbol");
    oop_handle oa = bs->find_global("OrderedArray");

    bs->create_globals_dict();

    oop_handle sc2 = bs->find_global("Symbol");
    oop_handle oa2 = bs->find_global("OrderedArray");

    Print(sc.repr());
    Print(bs->symclass().repr());
    assert(sc == bs->symclass());

    Print("** Creating Symbol#symbols ...");
    bs->create_symbol_symbols();
    {
        const oop_handle& ss = bs->symbols_array();
        // for(int i = 1; i <= ss.get_obj()->num_slots(); ++i)
        //     Print(i << ": #" << ss.get_obj()->get_field(i).get_obj()->cstr());
    }

    Print("** Done");


    bs->run_code(
        "def Object inSlot: slot put: value\n"
        "[ <0 self slot value> ].\n"
        "(3 + 4) print.\n"
        //"Object define: #inSlot:put: as: [|:slot :value| <0 self slot value>]"
    );

    return sp;
}


void testvm2()
{

    auto sp = oop_space::create(1 << 16);
    auto bs = new interp::bootstrapper(sp);
    auto o1 = sp->alloc(8, false);
    SHOWSRC( o1->get_size() );
    SHOWSRC( o1->get_refcount() );
    /*
    {
    oop_handle o(o1);
    SHOWSRC(o.get<TAG_OBJ>()->get_refcount());
    assert(o1->get_refcount() == 1);
    oop_handle o2(o);
    SHOWSRC(o.get<TAG_OBJ>()->get_refcount());
    assert(o1->get_refcount() == 2);
    o2 = oop_handle::nil_obj;
    SHOWSRC(o.get<TAG_OBJ>()->get_refcount());
    assert(o1->get_refcount() == 1);

    o.get<TAG_OBJ>()->set_field(1, o);
    SHOWSRC(o.get<TAG_OBJ>()->get_refcount());
    assert(o1->get_refcount() == 2);
    o.get<TAG_OBJ>()->set_field(1, oop_handle::nil_obj);
    assert(o1->get_refcount() == 1);
    }*/
    {
        oop_handle o(o1);
        SHOWSRC(o1->get_refcount());

    }

    //o1->set_field(1, oop_handle(o1));
    SHOWSRC( o1->get_refcount() );

    cout << "-----" << endl;



    //bs->strapboots();
    //bs->t1();
}

void testvm()
{

    std::map<std::string, oop_handle> foo;
    foo.emplace("a", oop_handle::nil_obj);
    auto x = foo.find("b");
    if(x != foo.end()) assert(0);

    std::cout << "it works in main.cpp" << std::endl;



    auto sp = oop_space::create(1 << 16);
    auto bs = new interp::bootstrapper(sp);
    bs->strapboots("bootstrap");
    bs->t1();


    obj_ptr xxx;
    assert(xxx.is_null());
    assert((obj_ptr::offset_t)xxx == 1);

}


void testallocs()
{

    auto sp = oop_space::create(512);
    auto my_p = sp->alloc(1, true);

    using std::cout;
    using std::endl;

    cout << my_p.is_null() << endl;
    cout << my_p.get_offset() << endl;

    oop_handle xx;
    //xx.set<TAG_OBJ>(nil_obj);
    xx = oop_handle::mk(42);
    SHOWSRC( xx.get_tag() );
    if(xx.get_tag() == TAG_OBJ)
        SHOWSRC( xx.get<TAG_OBJ>().get_offset() );
    else
        SHOWSRC( xx.get<TAG_INT>() );

    cout << find_index<uint32_t, int32_t, uint32_t>::value << endl;
    cout << find_index<int32_t, int32_t, uint32_t>::value << endl;

    cout << oop_handle::tag_t::find<int64_t> << endl;
    cout << oop_handle::tag_t::find<my_abs_ptr<oop_obj>> << endl;

    struct zzz
    {
        int foo;
        int bar[0];
    };
    SHOWSRC( sizeof(zzz) );

    oop_handle hh;
    SHOWSRC( hh.get_tag() );
    SHOWSRC( hh.get_tag() == oop_handle::tags::Nil );
    // SHOWSRC( hh.get_tag() & oop_handle::tags::Falsy );

    //oop_class c;

    //auto my_obj = vm.new_object()

    //interp::builder b;
    //b.push(3);
    //b.push(4);
    //b.send("+", 1);




    // need to set up Object





}