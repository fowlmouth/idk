#pragma once
#include "ast.hpp"
#include "aux.hpp"
#include "grammar.hpp"

#ifdef CodegenTrace
#define dbg_bytes(b) \
    { std::cout << "[";\
    for(int i = 0; i < b.size(); ++i) std::cout << (int)b[i] << ','; \
        std::cout << std::endl; \
    } while(0)
#define dbg_trace(fn) Print(" codegen: " #fn);

#else
#define dbg_bytes(b)
#define dbg_trace(fn)
#endif

namespace vm {

class codegen;

class block_builder
{
    std::vector<unsigned char> bytes;
    int instr_count;

    block_builder* parent;

    std::vector<std::string> locals;
    std::size_t num_args;


public:
    struct find_result
    {
        enum{result_none, result_local, result_slot} kind;
        union{
            vm::bytecode::upvalue local;
            std::size_t slot;
        };

        find_result(): kind(result_none) {}
    };
    bool find (const std::string& s, find_result& out, std::size_t offs)
    {
        const auto& fi = std::find(locals.begin(), locals.end(), s);
        if(fi == locals.end())
        {
            if(parent)
                return parent->find(s, out, offs+1);
            return find_ivar(s, out);
        }
        out.kind = find_result::result_local;
        out.local = vm::bytecode::upvalue{offs, 1+(std::size_t)(fi-locals.begin())};
        return true;
    }

    bool find (const std::string& s, find_result& out)
    {
        if(s[0] == '@')
            return find_ivar(s.substr(1, s.size()), out);
        return find(s, out, 0);
    }

    bool find_ivar (const std::string& s, find_result& out)
    {
        int slot = class_find_instvar(klass, s.c_str());
        if(slot == -1)
        {
            out.kind = find_result::result_none;
            return false;
        }
        out.kind = find_result::result_slot;
        out.slot = slot;
        return true;
    }

    const oop_handle& method, &klass;

    block_builder(block_builder* parent)
    :   parent(parent),
        klass(parent->klass),
        method(parent->method)
    {}

    block_builder(const oop_handle& method, const oop_handle& klass)
    :   method(method),
        klass(klass),
        parent(nullptr)
    {
    }

    std::vector<std::string>& get_locals() { return locals; }
    const std::vector<unsigned char>& get_bytes() { return bytes; }


    using opcode = vm::opcode;

    void pop ()
    {
        dbg_trace(pop)
        bytes.push_back(opcode::pop);
        instr_count++;
    }
    void push (int32_t num)
    {
        dbg_trace(push_i32)
        bytes.push_back(opcode::push_i32);
        _write_bytes(&num);
        instr_count++;
    }
    void push_nil()
    {
        dbg_trace(push_nil)
        bytes.push_back(opcode::push_nil);
        instr_count++;
    }
    void push_true()
    {
        dbg_trace(push_true)
        bytes.push_back(opcode::push_true);
        instr_count++;
    }
    void push_false()
    {
        dbg_trace(push_false)
        bytes.push_back(opcode::push_false);
        instr_count++;
    }
    void push_self()
    {
        dbg_trace(push_self)
        bytes.push_back(opcode::push_self);
        instr_count++;
    }
    void push_context()
    {
        dbg_trace(push_context)
        bytes.push_back(opcode::push_context);
        instr_count++;
    }
    void push_string(const std::string& s)
    {
        dbg_trace(push_string)
        bytes.push_back(opcode::push_string);
        uint16_t len = s.size();
        _write_bytes(&len);
        bytes.insert(bytes.end(), s.begin(), s.end());
        bytes.push_back('\0');
        instr_count++;
    }
    void push_array(uint16_t members)
    {
        dbg_trace(push_array)
        bytes.push_back(opcode::push_array);
        _write_bytes(&members);
        instr_count++;
    }
    void ret()
    {
        dbg_trace(ret)
        bytes.push_back(opcode::ret);
        instr_count++;
    }

    void send (const std::string& msg, int32_t num_args, int type = 0)
    {
        dbg_trace(send)
        assert(type >= 0);
        assert(type <= 2);
        bytes.push_back(opcode::send + type);
        _write_bytes(&num_args);
        uint16_t len = msg.size();
        _write_bytes(&len);
        bytes.insert(bytes.end(), msg.begin(), msg.end());
        bytes.push_back('\0');

        instr_count++;
    }
    void send_self (const std::string& msg, int32_t num_args)
    {
        dbg_trace(send_self)
        send(msg, num_args, 1);
    }
    void send_super(const std::string& msg, int32_t num_args)
    {
        dbg_trace(send_super)
        send(msg, num_args, 2);
    }

    void set_slot (std::size_t slot)
    {
        dbg_trace(set_slot)
        bytes.push_back(opcode::set_slot);
        int32_t s = slot;
        _write_bytes(&s);
        instr_count++;
    }
    void set_upvalue(uint16_t ups, uint16_t local_id)
    {
        dbg_trace(set_upvalue)
        bytes.push_back(opcode::set_upvalue);
        _write_bytes(&ups);
        _write_bytes(&local_id);
        instr_count++;
    }
    void get_slot (std::size_t slot)
    {
        dbg_trace(get_slot)
        bytes.push_back(opcode::get_slot);
        int32_t s = slot;
        _write_bytes(&s);
        instr_count++;
    }
    void get_upvalue(uint16_t ups, uint16_t index)
    {
        dbg_trace(get_upvalue)
        bytes.push_back(opcode::get_upvalue);
        _write_bytes(&ups);
        _write_bytes(&index);
        instr_count++;
    }

    void jump(uint16_t loc, opcode opc = opcode::jump)
    {
        dbg_trace(jump)
        bytes.push_back(opc);
        _write_bytes(&loc);
        instr_count++;
    }

    int ip() const
    {
        return bytes.size();
    }

    struct label
    {
        std::vector<uint16_t> uses;
        uint16_t loc;
    };
    void jump(label& label, opcode opc = opcode::jump)
    {
        dbg_trace(jump)
        bytes.push_back(opc);
        label.uses.push_back(ip());
        uint16_t loc = 0;
        _write_bytes(&loc);
        instr_count++;
    }
    void clear (label& label)
    {
        unsigned char* loc = (unsigned char*)&label.loc;
        Print0( "@ " << label.loc );
        for(auto i: label.uses)
        {
            bytes[i] = loc[0];
            bytes[i+1] = loc[1];
            Print0( "  " << i );
        }
    }


    void halt ()
    {
        dbg_trace(halt)
        bytes.push_back(opcode::halt);
        instr_count++;
    }

    void primitive(std::size_t idx, std::size_t num_args)
    {
        dbg_trace(primitive)
        bytes.push_back(opcode::call_primitive);
        _write_bytes(&idx);
        _write_bytes(&num_args);
        instr_count++;
    }

    void load_symbol(std::string name)
    {
        dbg_trace(load_symbol)
        bytes.push_back(opcode::load_symbol);
        bytes.insert(bytes.end(), name.begin(), name.end());
        bytes.push_back('\0');
        instr_count++;
    }
    void load_const(std::string name)
    {
        dbg_trace(load_const)
        bytes.push_back(opcode::load_const);
        bytes.insert(bytes.end(), name.begin(), name.end());
        bytes.push_back('\0');

        instr_count++;
    }
    void method_end()
    {
        dbg_trace(method_end)
        bytes.push_back(opcode::method_end);
        instr_count++;
    }
    void block_end()
    {
        dbg_trace(block_end)
        bytes.push_back(opcode::block_end);
        instr_count++;
    }

    void push_block(block_builder* block)
    {
        dbg_trace(push_block)
        bytes.push_back(opcode::push_block);
        const auto& bc = block->bytes;
        vm::bytecode::block_header header;
        header.len = bc.size();
        header.locals = block->get_locals().size();
        bytes.insert(bytes.end(), (unsigned char*)&header, (unsigned char*)(&header + 1));
        bytes.insert(bytes.end(), block->bytes.begin(), block->bytes.end());
        instr_count++;
    }

    template<typename T>
    inline void _write_bytes (const T* value)
    {
        // bytes.insert(bytes.end(), (unsigned char*)value, (unsigned char*)(value+1));
        _write_bytes((unsigned char*)(value), (unsigned char*)(value+1));
    }

    inline void _write_bytes (unsigned char* begin, unsigned char* end)
    {
        bytes.insert(bytes.end(), begin, end);
    }


};







class codegen: public boost::static_visitor<>
{
    block_builder* context;

public:
    codegen (block_builder* context):
        context(context)
    {
    }
    ~codegen ()
    {
        delete context;
    }

    const std::vector<unsigned char>& get_bytes()
    {
        return context->get_bytes();
    }

    template<class X>
    void operator() (X&) const
    {
        std::cout << "codegen not implemented "
                  << demangled_name<X>() << std::endl;
    }

    void operator() (ast::variable& v) const
    {
        block_builder::find_result res;
        if(context->find(v.name, res))
        {
            if(res.kind == block_builder::find_result::result_local)
                context->get_upvalue(res.local.parents, res.local.index);
            else
                context->get_slot(res.slot);
        }
        else
            PrintError(v.name << " not found!");
    }

    void operator()(ast::instvar& v) const
    {
        block_builder::find_result res;
        if(context->find_ivar(v.name, res))
            context->get_slot(res.slot);
        else
            PrintError("@"<<v.name << " not found!");
    }

    void operator() (ast::constant& c) const
    {
        context->load_const(c.name);
    }
    void operator() (ast::symbol& s) const
    {
        context->load_symbol(s.name);
    }

    void operator() (ast::intlit& i) const
    {
        context->push((int32_t)i.value);
    }
    void operator() (ast::nil&) const
    {
        context->push_nil();
    }

    void operator() (ast::truelit&) const
    {
        context->push_true();
    }
    void operator() (ast::falselit&) const
    {
        context->push_false();
    }

    void operator() (ast::thisContext&) const
    {
        context->push_context();
    }

    void operator() (ast::self&) const
    {
        context->push_self();
    }
    void operator() (ast::stringlit& s) const
    {
        context->push_string(s.name);
    }

    void operator() (ast::array& x)  const
    {
        for(auto& m: x.members)
            boost::apply_visitor(*this, m);
        context->push_array(x.members.size());
    }

    void operator() (ast::ret& xpr) const
    {
        boost::apply_visitor(*this, xpr.value);
        context->ret();
    }

    void operator() (ast::assign& xpr) const
    {
        block_builder::find_result res;
        if(context->find(xpr.dest, res))
        {
            boost::apply_visitor(*this, xpr.value);
            if(res.kind == block_builder::find_result::result_local)
                context->set_upvalue(res.local.parents, res.local.index);
            else
                context->set_slot(res.slot);
            return;
        }
        PrintError("variable not found: " << xpr.dest);

    }



    void operator() (ast::call_primitive& p) const
    {
        for(auto arg: p.args)
        {
            boost::apply_visitor(*this, arg);
        }
        context->primitive(p.id, p.args.size());
    }

    struct special_msg
    {
        std::function<bool(ast::message& m)> filter;
        std::function<void(ast::message& m, const codegen* cg)> action;
    };


    static std::vector<special_msg> inlinable_calls;
    static const special_msg* inlinable_call(ast::message& m)
    {
        for(const auto& c: inlinable_calls)
            if(c.filter(m))
                return &c;
        return 0;
    }
    static void init();

    void operator() (ast::message& m) const
    {
        auto fn = inlinable_call(m);
        if(fn)
        {
            fn->action(m, this);
            return;
        }

        //ast_print()(m);

        // dbg_bytes(context->get_bytes());
        enum{send_default = 0, send_self, send_super}
            mode = send_default;
        if(boost::get<ast::self>(&m.args[0])) mode = send_self;
        else if(boost::get<ast::super>(&m.args[0])) mode = send_super;

        for(int idx = (mode > 0) ? 1 : 0; idx < m.args.size(); ++idx)
            boost::apply_visitor(*this, m.args[idx]);

        context->send(m.name.c_str(), m.args.size() - 1, mode);
        // dbg_bytes(context->get_bytes());
    }

    void operator() (ast::block& b) const
    {
        auto ctx = new block_builder(context);
        auto& locals = ctx->get_locals();
        locals.insert(locals.end(), b.args.begin(), b.args.end());
        locals.insert(locals.end(), b.locals.begin(), b.locals.end());
        // auto visitor = codegen(ctx);
        // auto it = b.stmts.begin();
        // auto last = b.stmts.end();
        // while(it < last)
        // {
        //     boost::apply_visitor(visitor, *it);
        //     it++;
        //     if(it != last) ctx->pop();
        //     dbg_bytes(ctx->get_bytes());
        // }
        auto visitor = codegen(ctx);
        visitor.visit_statements(b.stmts);
        ctx->block_end();
        context->push_block(ctx);

    }

    void visit_statements (std::vector<ast::expr>& stmts) const
    {
        auto it = stmts.begin(), last = stmts.end();
        dbg_bytes(context->get_bytes());
        while(it < last)
        {
            boost::apply_visitor(*this, *it);
            it++;
            if(it != last) context->pop();
            dbg_bytes(context->get_bytes());
        }
    }

};

} // ::vm

/* inlined messages

these messages are inlined for control flow
and:, or:, ifTrue:else:, ifTrue:, ifFalse:,
whileTrue:, loopForever

these operators are inlined as opcodes for integers
+, -, *, /, %, <, <=, >, >=, ==, !=
*/

std::vector< vm::codegen::special_msg> vm::codegen::inlinable_calls;
void vm::codegen::init()
{
    auto inlinable_block = [](ast::expr& expr)->bool{
        auto b = boost::get<ast::block>(&expr);
        return b && b->total_locals() == 0;
    };
    auto inline_block = [=](ast::expr& expr, const codegen* cg){
        if(inlinable_block(expr))
            cg->visit_statements(boost::get<ast::block>(expr).stmts);
        else
        {
            // add a message expression "value" to the expression to call the block
            ast::message m{.name = "value", .args = {expr}};
            // m.name = "value";
            // m.args.push_back(expr);
            // visit the expression
            // boost::apply_visitor(*cg, m); 
            cg->operator()(m);
        }
    };

    // inlinable_calls.push_back(special_msg{
    //     [=](ast::message& m)->bool{
    //         return m.name == "ifTrue:else:" ;
    //     },
    //     [=](ast::message& m, const codegen* cg){
    //         /* x ifTrue: y else: z
    //         =>  (x)
    //             if-false-jump else_branch
    //             (y)
    //             jump post_if
    //             else_branch:
    //             (z)
    //             post_if:
    //         */

    //         auto& cond = m.args[0];
    //         auto& branch = //boost::get<ast::block>
    //             (m.args[1]);
    //         auto& else_  = //boost::get<ast::block>
    //             (m.args[2]);
    //         block_builder::label else_branch, post_if;

    //         boost::apply_visitor(*cg, cond);
    //         cg->context->jump(else_branch, opcode::jump_iff);
    //         // cg->visit_statements(branch.stmts);
    //         inline_block(branch, cg);
    //         cg->context->jump(post_if);
    //         //else branch starts here
    //         else_branch.loc = cg->context->ip();
    //         // cg->visit_statements(else_.stmts);
    //         inline_block(else_, cg);
    //         post_if.loc = cg->context->ip();

    //         // fix label1 and label2 in the bytecode
    //         // ...
    //         cg->context->clear(else_branch);
    //         cg->context->clear(post_if);


    //         //ast_print()(m);//, m);
    //     }
    // });

    inlinable_calls.push_back(special_msg{
        [=](ast::message& m)->bool{
            return m.name == "ifTrue:" ;
        },
        [=](ast::message& m, const codegen* cg){
            /* x ifTrue: y
                (x)
                peek-if-false-jump post
                (y)
                post:
            */
            auto& cond = m.args[0];
            auto& branch = //boost::get<ast::block>
                (m.args[1]);
            block_builder::label post;

            boost::apply_visitor(*cg, cond);
            cg->context->jump(post, opcode::peek_jump_iff);
            // cg->visit_statements(branch.stmts);
            inline_block(branch, cg);
            post.loc = cg->context->ip();

            cg->context->clear(post);

        }
    });

    // inlinable_calls.push_back(special_msg{
    //     [=](ast::message& m)->bool{
    //         return m.name == "ifFalse:";
    //     },
    //     [=](ast::message& m, const codegen* cg){
    //         /* a ifFalse: b
    //             (a)
    //             peek-if-true-jump post
    //             (b)
    //             post:
    //         */
    //         auto& c = m.args[0];
    //         auto& f = m.args[1];
    //         block_builder::label post;

    //         boost::apply_visitor(*cg, c);
    //         cg->context->jump(post, opcode::peek_jump_ift);
    //         inline_block(f, cg);
    //         post.loc = cg->context->ip();

    //         cg->context->clear(post);
    //     }
    // });

    inlinable_calls.push_back(special_msg{
        [=](ast::message& m)->bool{
            return m.name == "and:" ;//&& inlinable_block(m.args[1]);
        },
        [=](ast::message& m, const codegen* cg){
            /* a and: [b]
                (a)
                peek-if-false-jump post_cond
                (b)
                post_cond:
            */
            auto& a = m.args[0];
            auto& b = //boost::get<ast::block>
                (m.args[1]);
            block_builder::label post_cond_;

            boost::apply_visitor(*cg, a);
            cg->context->jump(post_cond_, opcode::peek_jump_iff);
            // cg->visit_statements(b.stmts);
            inline_block(b, cg);
            post_cond_.loc = cg->context->ip();

            cg->context->clear(post_cond_);

        }
    });

    inlinable_calls.push_back(special_msg{            
        [=](ast::message& m)->bool{
            return m.name == "or:" ;// && inlinable_block(m.args[1]);
        },
        [=](ast::message& m, const codegen* cg){
            /* a or: [b]
                (a)
                peek-if-true-jump post_cond
                (b)
                post_cond:
            */
            auto& a = m.args[0];
            auto& b = //boost::get<ast::block>
                (m.args[1]);
            block_builder::label post_cond_;

            boost::apply_visitor(*cg, a);
            cg->context->jump(post_cond_, opcode::peek_jump_ift);
            // cg->visit_statements(b.stmts);
            inline_block(b, cg);
            post_cond_.loc = cg->context->ip();

            cg->context->clear(post_cond_);
        }
    });

    inlinable_calls.push_back(special_msg{            
        [=](ast::message& m)->bool{
            return m.name == "whileTrue:" ;//&& inlinable_block(m.args[0]) && inlinable_block(m.args[1]);
        },
        [=](ast::message& m, const codegen* cg){
            /* a whileTrue: b
                condition:
                (a)
                if-false-jump post
                (b)
                pop
                jump condition
                post:
                nil
            */
            auto& a = //boost::get<ast::block>
                (m.args[0]);
            auto& b = //boost::get<ast::block>
                (m.args[1]);
            block_builder::label condition, body, post;

            condition.loc = cg->context->ip();
            // cg->visit_statements(a.stmts);
            inline_block(a, cg);
            cg->context->jump(post, opcode::jump_iff);
            // cg->visit_statements(b.stmts);
            inline_block(b, cg);
            cg->context->pop();
            cg->context->jump(condition, opcode::jump);
            post.loc = cg->context->ip();
            cg->context->push_nil();

            cg->context->clear(condition);
            cg->context->clear(post);

        }
    });

    inlinable_calls.push_back(special_msg{            
        [=](ast::message& m)->bool{
            return m.name == "loopForever" ;//&& inlinable_block(m.args[0]);
        },
        [=](ast::message& m, const codegen* cg){
            /* x loopForever
                pre:
                (x)
                pop
                jump pre
                post: // unreachable
                nil
            */
            auto& block = //boost::get<ast::block>
                (m.args[0]);
            block_builder::label pre,post;

            pre.loc = cg->context->ip();
            // cg->visit_statements(block.stmts);
            inline_block(block, cg);
            cg->context->pop();
            cg->context->jump(pre.loc, opcode::jump);
            post.loc = cg->context->ip();
            cg->context->push_nil();

            cg->context->clear(pre);
            cg->context->clear(post);

        }
    });

    inlinable_calls.push_back(special_msg{
        [](ast::message& m)->bool{
            auto c = m.name[0];
            return c == '+' || c == '-' || c == '*' || c == '/' || c == '%' || c == '<' || c == '>' || m.name == "==" || m.name == "!=";
        },
        [](ast::message& m, const codegen* cg){
            boost::apply_visitor(*cg, m.args[0]);
            boost::apply_visitor(*cg, m.args[1]);

            auto c = m.name[0];
            opcode op;
            switch(c)
            {
            case '+': op = opcode::add; break;
            case '-': op = opcode::sub; break;
            case '*': op = opcode::mul; break;
            case '/': op = opcode::div; break;
            case '<': switch(m.name[1]){
                case '<': op = opcode::shl; break;
                case '=': op = opcode::lteq; break;
                default: op = opcode::lt;
            }; break;
            case '>': switch(m.name[1]){
                case '>': op = opcode::shr; break;
                case '=': op = opcode::gteq; break;
                default: op = opcode::gt;
            }; break;
            case '%': op = opcode::mod; break;
            case '=': op = opcode::eq; break;
            case '!': op = opcode::neq; break;
            };
            cg->context->_write_bytes(&op);
        }
    });

    // inlinable_calls.push_back(special_msg{            
    //     [](ast::message& m)->bool{
    //         return m.name == "ifNil:";
    //     },
    //     [](ast::message& m, const codegen* cg){
    //         /* x ifNil: [y]
    //           (x)


    //     }
    // });

    // inlinable_calls.push_back(special_msg{            
    //     [](ast::message& m)->bool{}
    //     [](ast::message& m, const codegen* cg){}
    // });

}


#undef dbg_trace
#undef dbg_trace
