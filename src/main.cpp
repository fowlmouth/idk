
#define SHOWSRC(xp) (std::cout << (#xp ": ") << (xp) << std::endl)



#include <iostream>
#include <cstdint>
#include <cstdlib>
#include <functional>
#include <map>
#include <string>
#include <fstream>
#include <streambuf>

#include "defs.hpp"
#include "linkedlist.hpp"
#include "absptr.hpp"
#include "tagged.hpp"
#include "tests.hpp"


#include "obj.hpp"
#include "interp.hpp"
#include "grammar.hpp"
#include "evaluator.hpp"

#include "bootstrapper.hpp"
#include "codegen.hpp"
#include "parser_tests.hpp"
#include "bootstrapper_tests.hpp"
#include "cxxopts.hpp"


#ifdef EnableNVG
#include "nvg_visualizer.hpp"
#endif



#ifdef EnableLineNoise
#include "../linenoise/linenoise.h"
std::string readline (const char* prompt)
{
    auto ln = linenoise(prompt);
    if(!ln) return "";
    std::string res(ln);
    free(ln);
    return res;
}
#else
std::string readline (const char* prompt)
{
    std::cout << prompt;
    std::string res;
    std::getline(std::cin, res);
    return res;
}
#endif

bool file_exists (const char* path)
{
    struct stat s;
    return stat(path, &s) == 0;
}



// static oop_handle x_file (const std::string& file)
// {
//     std::vector<std::string> code;
//     else
//     {
//         std::ifstream f(file);
//         std::stringstream buf;
//         buf << f.rdbuf();
//         code.push_back(buf.str());
//     } 

//     oop_handle x;
//     for(auto& s: code)
//         x = interp::evaluator::eval(s);
//     return x;
// }

int main(int argc, char **argv)
{
    std::string
        image = "",
        bootstrap_file,
        linenoise_history_file = "replhistory.log";
    unsigned image_size = 1 << 26;

#ifdef EnableLineNoise
    linenoiseHistoryLoad(linenoise_history_file.c_str());
#endif

    cxxopts::Options opt(argv[0], "command line opts");
    opt.add_options()
        ("h,help", "Print help")
        ("i,image", "file", cxxopts::value<std::string>())
        ("e,exec", "execute string", cxxopts::value< std::string>())
        ("f,file", "read source file", cxxopts::value< std::vector< std::string>>())
        ("b,bootstrap", "bootstrap file", cxxopts::value<std::string>())
        ("parser_tests", "tests", cxxopts::value<bool>())
        ("image_size", "bootstrap", cxxopts::value<unsigned>())
        ("repl", "repl", cxxopts::value<bool>())
        ("mmap", "use mmap to load image", cxxopts::value<bool>())
        ("nosave", "don't save the image", cxxopts::value<bool>())
#ifdef EnableLineNoise
        ("keycodes","", cxxopts::value<bool>())
        ("multiline","",cxxopts::value<bool>())
#endif
#ifdef DynamicPrint
        ("p,printlevel", "debug", cxxopts::value<int>())
#endif
    ;
    opt.parse(argc, argv);
#ifdef EnableLineNoise
    linenoiseSetMultiLine(opt.count("multiline") > 0);
    if(opt.count("keycodes")) linenoisePrintKeyCodes();
#endif

    if (opt.count("h"))
    {
      std::cout << opt.help({"", "Group"}) << std::endl;
      exit(0);
    }

    vm::codegen::init();

    if(opt.count("i"))
        image = opt["i"].as<std::string>();
    if(opt.count("b"))
        bootstrap_file = opt["b"].as<std::string>();
#ifdef DynamicPrint
    if(opt.count("p"))
        print_level = opt["p"].as<int>();
#endif

    if(opt.count("parser_tests"))
        tests::run_parser_tests();
    if(opt.count("image_size"))
        image_size = opt["image_size"].as<unsigned>();
    bool mmap = opt.count("mmap");

    oop_space* sp;
    if(!image.empty() && file_exists(image.c_str()))
    {
        Print("loading " << image);
        sp = (mmap
            ? oop_space::mmap_image(image.c_str())
            : oop_space::read_file(image.c_str()) );
    }
    else if(!bootstrap_file.empty())
    {
        sp = oop_space::create(image_size);
        interp::bootstrapper(sp).strapboots(bootstrap_file);
    }
    else
        return 1;

    if(opt.count("f"))
    {
        auto& files = opt["f"].as< std::vector< std::string>>();
        for(auto& f: files)
            interp::evaluator::eval_file(f);
    }
    if(opt.count("e"))
    {
        auto& code = opt["e"].as< std::string>();
        auto res = interp::evaluator::eval(code);
        Print0(res.repr());
    }

    bool bsave_image = true;
    if(opt.count("nosave")) bsave_image = false;

    auto save_image = [&](){
        if(!image.empty() && !mmap)
            sp->write_file(image.c_str());
    };

    if(opt.count("repl"))
    {
        auto show_help = [](){
            Print0("Commands:\n  !read filename\n  !save - saves the image\n  !clear - clears the input buffer\n  !help - show this screen\n  !quit");
        };
        show_help();

        std::string input;
        while(true)
        {
            using Iter = std::string::const_iterator;
            std::string str = readline("> ");
            if(std::cin.eof())
                break;
            if(str == "!clear")
                input.clear();
            else if(str == "!quit")
                break;
            else if(str == "!save")
                save_image();
            else if(str == "!help")
                show_help();
            else if(0 == strncmp(str.c_str(), "!read ", 6))
            {
                std::string file = str.substr(6);
                Print0("evaluating " << file);
                interp::evaluator::eval_file(file);
            }
            else
            {
                input.append(str);
#ifdef EnableLineNoise
                linenoiseHistoryAdd(str.c_str());
#endif

                interp::bootstrapper::toplevel_stmts stmts;
                oop_handle result;
                if(parse_toplevel(input.begin(), input.end(), stmts))
                {
                    for(auto& s: stmts)
                        result = interp::evaluator::eval(s);
                    input.clear();
                    Print0(result.repr());
                }
            }
        }

    }

    if(bsave_image)
        save_image();

    int res = 0;
#ifdef EnableNVG
    res = main_handler<>(sp);
#else

#endif
    if(mmap)
        munmap(sp->start, sp->size);
#ifdef EnableLineNoise
    linenoiseHistorySave(linenoise_history_file.c_str());
#endif

    return res;

}
