#pragma once
#include "obj.hpp"
#include "vm_opcodes.hpp"
#include "primitives.hpp"
#include "ast.hpp"
#include "codegen.hpp"
#include "compiler.hpp"
#include "pmc.hpp"

#include <cstring>
#include <vector>
#include <map>
#include <iomanip>
#include <cctype>
#include <algorithm>


using std::cout;
using std::endl;



namespace interp
{


oop_handle create_block_context (const oop_handle& parent, INT_TYPE ip, INT_TYPE locals)
{
    auto sp = oop_space::get_instance();
    auto p = alloc_slots(block_context::slots, sp->globals("Block"));
    const oop_handle& array_class = sp->globals("Array");
    // p->set_field(0, dict_search(sp->globals(), "Block"));
    p->set_field(block_context::ip, oop_handle(ip));
    p->set_field(block_context::low_ip, oop_handle(ip));
    p->set_field(block_context::sp, oop_handle((INT_TYPE)0));
    p->set_field(block_context::stack,
        oop_handle(alloc_slots(32, array_class)));
    p->set_field(block_context::method, parent.get_obj()->get_field(context::method));
    p->set_field(block_context::parent, parent);
    p->set_field(block_context::locals, oop_handle(alloc_slots(locals, array_class)));
    return oop_handle(p);
}




const oop_handle& symbols_array()
{
    return oop_space::get_instance()->sym_class().get_obj()->get_field(oop_class_slots::slots+1);
}
const oop_handle& new_sym (const char* name)
{
    auto idx = ordered_search(symbols_array(), name);
    if(idx > 0 && idx <= symbols_array().get_obj()->num_slots())
    {
        const oop_handle& s = symbols_array().get_obj()->get_field(idx);
        if(cmp_symbol( s.get_obj()->cstr(), name ) == 0)
            return s;
    }
    oop_handle obj = new_bits(name, strlen(name)+1, oop_space::get_instance()->sym_class());
    oop_space::get_instance()->sym_class().get_obj()->set_field(
        oop_class_slots::slots+1,
        array_insert( symbols_array(), idx, obj ));
    return symbols_array().get_obj()->get_field(idx);
}

oop_handle method_call_context (const oop_handle& recv, const char* msg, const oop_handle& caller,
    std::size_t n_args, oop_handle& args, const oop_handle& klass)
{
    auto sp = oop_space::get_instance();
    auto meth = lookup_method_class(klass, msg);

    oop_handle ctx;
    if(meth.is_nil())
    {
        ctx = create_method_context(recv,
            lookup_method_class(recv.get_class(), MISSING_MESSAGE_NAME),
            caller);
        args = oop_handle(alloc_slots(n_args, sp->globals("Array")));
        // obj_ptr args = alloc_slots(n_args, array_class);
        // for(int i = n_args; i > 0; --i)
        //     args->set_field(i, _pop);
        obj_ptr m = alloc_slots(message::slots, sp->globals("Message"));
        m->set_field(message::recv, recv);
        m->set_field(message::name, new_sym(msg));
        m->set_field(message::args, oop_handle(args));
        args = m->get_field(message::args);
        ctx.get_obj()->get_field(context::locals)
            .get_obj()->set_field(1, oop_handle(m));
    }
    else
    {
        ctx = create_method_context(recv,
            meth,
            caller);
        args = ctx.get_obj()->get_field(context::locals);
        // for(int i = n_args; i > 0; --i)
        //     ctx.get_obj()->get_field(context::locals).get_obj()->set_field(i, _pop);
    }
    return ctx;
}

oop_handle method_call_context (const oop_handle& recv, const char* msg, const oop_handle& caller,
    std::size_t n_args, oop_handle& args)
{
    return method_call_context(recv, msg, caller, n_args, args, recv.get_class());
}


class VM
{


public:
    static PMC pmc;

    oop_space* space;
    oop_handle process;

    friend class bootstrapper;


    VM (oop_space* space, oop_handle process): space(space), process(process)
    {
    }
    VM (oop_space* space): space(space)
    {
        obj_ptr p = alloc_slots(process::slots);
        process = oop_handle(p);

    }


    const oop_handle& result_value()
    {
        return process.get_obj()->get_field(process::result);
    }




    template<typename T>
    inline void read(const oop_handle& bitarray, INT_TYPE& ip, T& output)
    {
        output = *(T*)(bitarray.get_obj()->cstr() + ip);
        ip += sizeof(T);
    }
    template<typename T>
    inline T read(const oop_handle& bitarray, INT_TYPE& ip)
    {
        T res;
        read(bitarray, ip, res);
        return res;
    }

    enum return_status
    { error, finished, OK };
    return_status tick(int ticks = 0)
    {
        ticks = ticks || 8;

        const oop_handle& array_class = space->globals("Array");
        const oop_handle& message_class = space->globals("Message");

        oop_handle activeContext;

        oop_handle method;
        oop_handle bytecodes;

        INT_TYPE ip, sp;
        obj_ptr stack;

        auto save_ip_sp = [&]() {
            activeContext.get_obj()->set_field(context::ip, oop_handle(ip));
            activeContext.get_obj()->set_field(context::sp, oop_handle(sp));
        };
        auto set_context = [&](const oop_handle& context) {
            if(!activeContext.is_nil()) save_ip_sp();
            activeContext = context;
            process.get_obj()->set_field(process::context, activeContext);
            if(!activeContext.is_nil())
            {
                method = activeContext.get_obj()->get_field(context::method);
                bytecodes = method.get_obj()->get_field(oop_method_slots::bytecodes);

                ip = activeContext.get_obj()->get_field(context::ip).get_int();
                sp = activeContext.get_obj()->get_field(context::sp).get_int();
                stack = activeContext.get_obj()->get_field(context::stack).get_obj();
            }

        };
        auto save_context = [&]() {
            save_ip_sp();
            process.get_obj()->set_field(process::context, activeContext);
        };

        set_context(process.get_obj()->get_field(process::context));


        #define _push(obj) \
            stack->set_field(++sp, obj)
        #define _pop \
            stack->get_field(sp--)
        #define _top \
            stack->get_field(sp)
        #define _print_stack \
            {for(int i = 1; i <= stack->num_slots(); ++i)     \
                if(i > sp) break;                                      \
                else Print(i << ": " << stack->get_field(i).repr() ); \
            }while(0)

        auto method_parent = [&](){
            oop_handle ctx = activeContext;
            while(ctx.get_obj()->num_slots() == block_context::slots)
                ctx = ctx.get_obj()->get_field(block_context::parent);
            return ctx;
        };
        auto self = [&](){
            oop_handle ctx = method_parent();
            const oop_handle& locals = ctx.get_obj()->get_field(context::locals);
            const oop_handle& obj = locals.get_obj()->get_field(locals.get_obj()->num_slots());
            return obj;
        };

        for(; ticks; --ticks)
        {

            PrintCode(ip << ",," << sp);
            _print_stack;
            auto op = (vm::opcode)*(bytecodes.get_obj()->cstr() + ip++);
            PrintCode(vm::opcode_to_str(op));

            switch(op)
            {
            case vm::opcode::pop:
            {
                _pop;
            } break;

            case vm::opcode::ret:
            {
                auto obj = _pop;
                // set context to context.caller
                // auto caller = activeContext.get_obj()->get_field(context::caller);
                auto ctx = method_parent();
                set_context(ctx.get_obj()->get_field(context::caller));
                _push(obj);
            }
            break;

            case vm::opcode::method_end:
            {
                const auto& caller = activeContext.get_obj()->get_field(context::caller);
                // _print_stack;
                oop_handle val = _pop;
                PrintCode(val.repr());
                if(caller.is_nil())
                {
                    process.get_obj()->set_field(process::result, val);
                    // PrintCode(return_value);
                    // PrintCode(return_value.get_tag());
                    set_context(caller);
                    return finished;
                }
                set_context(caller);
                _push(val);

            } break;

            case vm::opcode::block_end:
            {
                const auto& caller = activeContext.get_obj()->get_field(context::caller);
                // _print_stack;
                oop_handle val = _pop;
                set_context(caller);
                _push(val);

            } break;

            case vm::opcode::push_i32:
            {
                int32_t n;
                read(bytecodes, ip, n);
                _push(oop_handle((INT_TYPE)n));
                // _print_stack;
            } break;

            case vm::opcode::push_nil:
                _push(oop_handle::nil_obj);
                break;

            case vm::opcode::push_self:
            {
                // oop_handle ctx = method_parent();
                //  activeContext;
                // while(ctx.get_obj()->num_slots() == block_context::slots)
                //     ctx = ctx.get_obj()->get_field(block_context::parent);
                // const oop_handle& locals = ctx.get_obj()->get_field(context::locals);
                const oop_handle& s = self(); // locals.get_obj()->get_field(locals.get_obj()->num_slots());
                PrintCode(s.repr());
                _push(s);
                break;
            }
            case vm::opcode::push_context:
                _push(activeContext);
                break;

            case vm::opcode::push_true:
                _push(oop_handle::true_obj);
                break;

            case vm::opcode::push_false:
                _push(oop_handle::false_obj);
                break;

            case vm::opcode::push_block:
            {
                vm::bytecode::block_header hdr;
                read(bytecodes, ip, hdr.len);
                read(bytecodes, ip, hdr.locals);
                auto start = bytecodes.get_obj()->cstr() + ip;
                //create block context
                auto block = create_block_context(activeContext, ip, hdr.locals);//, hdr.locals);
                ip += hdr.len;
                _push(block);

                //_push(oop_handle(p));

            } break;

            case vm::opcode::send:
            case vm::opcode::send_self:
            case vm::opcode::send_super:
            {
                //read the message
                auto n_args = read<int32_t>(bytecodes, ip);
                auto l_msg = read<uint16_t>(bytecodes, ip);
                const char* msg = bytecodes.get_obj()->cstr() + ip;
                ip += l_msg+1;
                Print("sending " << msg << "/" << n_args);

                const oop_handle& recv = (op == vm::opcode::send)
                    ? stack->get_field(sp - n_args)
                    : self();
                const oop_handle& klass = (op == vm::opcode::send_super)
                    ? method.get_obj()->get_field(oop_method_slots::klass)
                            .get_obj()->get_field(oop_class_slots::superclass)
                    : recv.get_class();

#ifdef EnableMethodCache
                oop_handle sym = new_sym(msg);
                obj_ptr method;
                bool is_method_missing = pmc.lookup(klass.get_obj(), sym.get_obj(), method);
                oop_handle ctx;
                if(is_method_missing)
                {
                    ctx = create_method_context(recv, oop_handle(method), activeContext);
                    obj_ptr args = alloc_slots(n_args, array_class);
                    for(int i = n_args; i > 0; --i)
                        args->set_field(i, _pop);
                    obj_ptr m = alloc_slots(message::slots, message_class);
                    m->set_field(message::recv, recv);
                    m->set_field(message::name, new_sym(msg));
                    m->set_field(message::args, oop_handle(args));
                    ctx.get_obj()->get_field(context::locals)
                        .get_obj()->set_field(1, oop_handle(m));
                }
                else
                {
                    ctx = create_method_context(recv, oop_handle(method), activeContext);
                    for(int i = n_args; i; --i)
                        ctx.get_obj()->get_field(context::locals).get_obj()->set_field(i, _pop);
                }
                if(op == vm::opcode::send) _pop;
                set_context(ctx);
                continue;
#else
                // auto meth = lookup_method_class(klass, msg);

                // oop_handle ctx;
                // if(meth.is_nil())
                // {
                //     ctx = create_method_context(recv,
                //         lookup_method_class(recv.get_class(), MISSING_MESSAGE_NAME),
                //         activeContext);
                //     obj_ptr args = alloc_slots(n_args, array_class);
                //     for(int i = n_args; i > 0; --i)
                //         args->set_field(i, _pop);
                //     obj_ptr m = alloc_slots(message::slots, message_class);
                //     m->set_field(message::recv, recv);
                //     m->set_field(message::name, new_sym(msg));
                //     m->set_field(message::args, oop_handle(args));
                //     ctx.get_obj()->get_field(context::locals)
                //         .get_obj()->set_field(1, oop_handle(m));
                // }
                // else
                // {
                //     ctx = create_method_context(recv,
                //         meth,
                //         activeContext);
                //     for(int i = n_args; i > 0; --i)
                //         ctx.get_obj()->get_field(context::locals).get_obj()->set_field(i, _pop);
                // }
                // if(op == vm::opcode::send) _pop; //recv
                // set_context(ctx);

                oop_handle args;
                oop_handle ctx = method_call_context(recv, msg, activeContext, n_args, args, klass);
                for(int i = n_args; i > 0; --i)
                    args.get_obj()->set_field(i, _pop);
                if(op == vm::opcode::send) _pop;
                set_context(ctx);
                continue;
#endif

            } break;

            case vm::opcode::call_primitive:
            {
                oop_handle val;
                std::size_t idx, args;
                read(bytecodes, ip, idx);
                read(bytecodes, ip, args);
                Print("** call primitive " << vm::primitive_to_str((vm::primitive)idx) << " (" << args << " args)");

                #define chkarg(n) if(args != n) goto early_return
                switch(idx)
                {
                case vm::primitive::obj_inslot_put:
                {
                    chkarg(3);
                    val = _pop;
                    const oop_handle &slot = _pop;
                    const oop_handle &obj = _pop;
                    obj.get_obj()->set_field(slot.get_int(), val);
                }   break;
                case vm::primitive::obj_inslot:
                {
                    chkarg(2);
                    const oop_handle &slot = _pop;
                    const oop_handle &obj = _pop;
                    val = obj.get_obj()->get_field(slot.get_int());
                }   break;
                case vm::primitive::obj_eqeq:
                {
                    chkarg(2);
                    const oop_handle &a = _pop;
                    const oop_handle &b = _pop;
                    val = ( (a == b) ? oop_handle::true_obj : oop_handle::false_obj );
                    break;
                }
                case vm::primitive::obj_send_args:
                {
                    chkarg(3);
                    const oop_handle& args = _pop;
                    const oop_handle& sym = _pop;
                    const oop_handle& recv = _pop;
                    int arg_count = args.is_nil() ? 0 : args.get_obj()->num_slots();
                    const oop_handle& method = lookup_method_class(recv.get_class(), sym.get_obj()->cstr());
                    if(method.is_nil())
                    {
                        obj_ptr msg = alloc_slots(message::slots, message_class);
                        msg->set_field(message::recv, recv);
                        msg->set_field(message::name, sym);
                        msg->set_field(message::args, args);
                        const oop_handle& m = lookup_method_class(recv.get_class(), "messageMissing:");
                        set_context( create_method_context(recv, m, activeContext.get_obj()->get_field(context::caller)) );
                        activeContext.get_obj()->get_field(context::locals)
                                .get_obj()->set_field(1, oop_handle(msg));
                    }
                    else
                    {
                        set_context( create_method_context(recv, method, activeContext.get_obj()->get_field(context::caller) ) );
                        if(!args.is_nil())
                            for(int i = 1; i <= arg_count; ++i)
                                activeContext.get_obj()->get_field(context::locals)
                                        .get_obj()->set_field(i, args.get_obj()->get_field(i));
                    }
                    continue;
                }
                case vm::primitive::obj_signal:
                    chkarg(2);
                    Print("error in result value");
                    process.get_obj()->set_field(process::result, _pop);
                    return error;

                case vm::primitive::int_add:
                case vm::primitive::int_sub:
                case vm::primitive::int_mul:
                case vm::primitive::int_div:
                case vm::primitive::int_mod:
                case vm::primitive::int_and:
                case vm::primitive::int_or:
                case vm::primitive::int_lt:
                case vm::primitive::int_shl:
                case vm::primitive::int_shr:
                {
                    chkarg(2);
                    INT_TYPE a, b;
                    b = _pop.get_int();
                    a = _pop.get_int();
                    switch(idx)
                    {
                    case vm::primitive::int_add:
                        val = oop_handle(a + b);
                        break;
                    case vm::primitive::int_sub:
                        val = oop_handle(a - b);
                        break;
                    case vm::primitive::int_mul:
                        val = oop_handle(a * b);
                        break;
                    case vm::primitive::int_div:
                        val = oop_handle(a / b);
                        break;
                    case vm::primitive::int_mod:
                        val = oop_handle(a % b);
                        break;
                    case vm::primitive::int_and:
                        val = oop_handle(a & b);
                        break;
                    case vm::primitive::int_or:
                        val = oop_handle(a | b);
                        break;
                    case vm::primitive::int_lt:
                        val = (a < b) ? oop_handle::true_obj : oop_handle::false_obj;
                        break;
                    case vm::primitive::int_shl:
                        val = oop_handle(a << b);
                        break;
                    case vm::primitive::int_shr:
                        val = oop_handle(a >> b);
                        break;
                    }

                }   break;

                case vm::primitive::obj_alloc:
                {
                    chkarg(2);
                    INT_TYPE slots = _pop.get_int();
                    val = oop_handle(alloc_slots(slots, _pop));
                }   break;
                case vm::primitive::byt_alloc:
                {
                    chkarg(2);
                    INT_TYPE bytes = _pop.get_int();
                    val = oop_handle(alloc_bits(bytes, _pop));
                }   break;
                case vm::primitive::byt_byteat:
                {
                    chkarg(2);
                    const oop_handle &slot = _pop;
                    const oop_handle &obj = _pop;
                    if(! slot.is_int() || ! obj.is_obj() || ! obj.rawget_obj()->is_bitarray()
                        || slot.rawget_int() < 1 || slot.rawget_int() > obj.rawget_obj()->get_size() )
                        goto early_return;
                    val = oop_handle( obj.rawget_obj()->bytes[ slot.rawget_int() - 1 ] );
                }   break;
                case vm::primitive::byt_byteat_put:
                {
                    chkarg(3);
                    val = _pop;
                    const oop_handle &slot = _pop;
                    const oop_handle &obj = _pop;
                    if(! slot.is_int() || ! obj.is_obj() || ! obj.get_obj()->is_bitarray()
                        || slot.rawget_int() < 1 || slot.rawget_int() > obj.rawget_obj()->get_size()
                        || ! val.is_int() )
                        goto early_return;
                    obj.rawget_obj()->bytes[ slot.rawget_int() - 1 ] = val.rawget_int() & 0xFF;
                }   break;
                case vm::primitive::byt_replace:
                {
                    chkarg(5);
                    const auto& base = _pop;
                    const auto& newstr = _pop;
                    const auto& fin = _pop;
                    const auto& start = _pop;
                    const auto& obj = _pop;
                    if(!obj.is_obj() || !newstr.is_obj() ||
                        ! obj.rawget_obj()->is_bitarray() || ! newstr.rawget_obj()->is_bitarray() ||
                        ! start.is_int() || ! fin.is_int() || !base.is_int())
                        goto early_return;
                    INT_TYPE st = start.rawget_int();
                    // INT_TYPE size = 1 + fin.rawget_int() - st;
                    Print3("byt_replace " << st << " to " << fin.rawget_int() << " base " << base.rawget_int());
                    memcpy(
                        &obj.get_obj()->bytes[st-1] ,
                        &newstr.get_obj()->bytes[base.rawget_int()-1] ,
                        1+ fin.rawget_int() - st);
                    val = obj;
                }   break;
                case vm::primitive::obj_size:
                {
                    chkarg(1);
                    const oop_handle& obj = _pop;
                    val = oop_handle(obj.get_obj()->num_slots());
                }   break;
                case vm::primitive::byt_size:
                    chkarg(1);
                    val = oop_handle(_pop.get_obj()->get_size());
                    break;

                case vm::primitive::block_activate:
                {
                    const oop_handle& self = _pop;
                    for(int i = args-1; i > 0; --i)
                        self.get_obj()->get_field(block_context::locals)
                            .get_obj()->set_field(i, _pop);
                    self.get_obj()->set_field(block_context::caller,
                        activeContext.get_obj()->get_field(context::caller));
                    self.get_obj()->set_field(block_context::ip,
                        self.get_obj()->get_field(block_context::low_ip));
                    set_context(self);
                    continue;
                }
                case vm::primitive::process_tick:
                {
                    chkarg(2);
                    INT_TYPE ticks = _pop.get_int();
                    VM vm(space, _pop);
                    auto res = vm.tick(ticks);
                    set_context(activeContext.get_obj()->get_field(context::caller));
                    _push(oop_handle(res));
                }   break;

                case vm::primitive::str_print:
                {
                    chkarg(1);
                    const oop_handle& str = _pop;
                    Print0(str.get_obj()->cstr());
                }   break;

                default:
                    Print("unknown primitive " << idx);
                    return error;
                    break;
                }

            normal_return:
                Print("reseting context to caller.. returning " << val.repr());
                set_context(activeContext.get_obj()->get_field(context::caller));
                _push(val);
                break;
            early_return:
                // primitive failed for some reason, continue in the context
                _push(val);
                break;

            }

            case vm::opcode::push_string:
            {
                auto len = read<uint16_t>(bytecodes, ip);
                const char* cs = bytecodes.get_obj()->cstr()+ip;
                ip += len + 1;
                _push(new_bits(cs, len+1, space->globals("String")));
            }   break;

            case vm::opcode::push_array:
            {
                uint16_t len;
                read(bytecodes, ip, len);
                obj_ptr val = alloc_slots(len, array_class);
                for(; len; --len)
                    val->set_field(len, _pop);
                _push(oop_handle(val));
            }   break;

            case vm::opcode::load_symbol:
            {
                const char* cs = bytecodes.get_obj()->cstr()+ip;
                ip += strlen(cs)+1;
                _push(new_sym(cs));

            }   break;

            case vm::opcode::load_const:
            {
                const char* cs = bytecodes.get_obj()->cstr()+ip;
                ip += strlen(cs) + 1;
                _push(space->globals(cs));

            }   break;

            case vm::opcode::load_immediate:
            {
                uint16_t idx;
                read(bytecodes, ip, idx);
                _push(method.get_obj()->get_field(idx));

            }   break;

            case vm::opcode::get_upvalue:
            {
                vm::bytecode::upvalue v;
                read(bytecodes, ip, v.parents);
                read(bytecodes, ip, v.index);
                obj_ptr scope = activeContext.get_obj();
                for(int i = 0; i < v.parents; ++i)
                    scope = scope->get_field(block_context::parent).get_obj();
                const auto& val = scope->get_field(block_context::locals).get_obj()->get_field(v.index);
                _push(val);
            }   break;

            case vm::opcode::set_upvalue:
            {
                vm::bytecode::upvalue v;
                read(bytecodes, ip, v.parents);
                read(bytecodes, ip, v.index);
                obj_ptr scope = activeContext.get_obj();
                for(; v.parents; --v.parents)
                    scope = scope->get_field(block_context::parent).get_obj();
                scope->get_field(block_context::locals).get_obj()->set_field(v.index, _top);

            }   break;

            case vm::opcode::get_slot:
            {
                int32_t slot;
                read(bytecodes, ip, slot);
                Print("** getslot " << slot << " from " << self().repr() );
                _push(self().get_obj()->get_field(slot));
            }   break;

            case vm::opcode::set_slot:
            {
                int32_t slot;
                read(bytecodes, ip, slot);
                self().get_obj()->set_field(slot, _top);
            }   break;

            case vm::opcode::jump:
            case vm::opcode::jump_ift:
            case vm::opcode::jump_iff:
            {
                uint16_t loc;
                Print("__" << ip);
                read(bytecodes, ip, loc);
                Print("__" << ip << " to " << loc);
                switch(op) {
                case vm::opcode::jump_ift:
                    if(_pop.is_truthy())
                        ip = loc;
                    break;
                case vm::opcode::jump_iff:
                    if(_pop.is_falsy())
                        ip = loc;
                    break;
                default:
                    ip = loc;
                }
            } break;

            // case vm::opcode::jump_ift:
            // {
            //     uint16_t loc;
            //     read(bytecodes, ip, loc);
            //     const auto& o = _pop;
            //     if(o.is_truthy())
            //         ip = loc;
            // }   break;

            // case vm::opcode::jump_iff:
            // {
            //     uint16_t loc;
            //     Print("__" << ip);
            //     read(bytecodes, ip, loc);
            //     Print("__" << ip << " to " << loc);
            //     const auto& o = _pop;
            //     if(o.is_falsy())
            //         ip = loc;
            // }   break;

            case vm::opcode::peek_jump_ift:
            {
                uint16_t loc;
                read(bytecodes, ip, loc);
                const auto& o = _top;
                if(o.is_truthy())
                    ip = loc;
                else
                    _pop;
            }   break;

            case vm::opcode::peek_jump_iff:
            {
                uint16_t loc;
                read(bytecodes, ip, loc);
                const auto& o = _top;
                if(o.is_falsy())
                    ip = loc;
                else
                    _pop;
            }   break;

            case vm::opcode::add: case vm::opcode::sub: 
            case vm::opcode::mul: case vm::opcode::div: 
            case vm::opcode::mod: case vm::opcode::eq:
            case vm::opcode::neq: case vm::opcode::lt:
            case vm::opcode::lteq: case vm::opcode::gt:
            case vm::opcode::gteq: case vm::opcode::shl:
            case vm::opcode::shr:
            {
                const oop_handle& xb = _pop;
                const oop_handle& xa = _pop;
                if(xa.is_int() && xb.is_int())
                {
                    auto a = xa.rawget_int();
                    auto b = xb.rawget_int();
                    oop_handle val;
                    switch(op){
                    case vm::opcode::add: val = oop_handle(a + b); break;
                    case vm::opcode::sub: val = oop_handle(a - b); break;
                    case vm::opcode::mul: val = oop_handle(a * b); break;
                    case vm::opcode::div: val = oop_handle(a / b); break;
                    case vm::opcode::mod: val = oop_handle(a % b); break;
                    case vm::opcode::eq:  val = oop_handle::boolean(a == b); break;
                    case vm::opcode::neq:  val = oop_handle::boolean(a != b); break;
                    case vm::opcode::lt:  val = oop_handle::boolean(a < b); break;
                    case vm::opcode::lteq: val = oop_handle::boolean(a <= b); break;
                    case vm::opcode::gt:  val = oop_handle::boolean(a > b); break;
                    case vm::opcode::gteq: val = oop_handle::boolean(a >= b); break;
                    case vm::opcode::shl: val = oop_handle(a << b); break;
                    case vm::opcode::shr: val = oop_handle(a >> b); break;
                    }
                    _push(val);
                }
                else
                {
                    // do a regular send

                    const char* msg;
                    switch(op){
                    case vm::opcode::add: msg = "+"; break;
                    case vm::opcode::sub: msg = "-"; break;
                    case vm::opcode::mul: msg = "*"; break;
                    case vm::opcode::div: msg = "/"; break;
                    case vm::opcode::mod: msg = "%"; break;
                    case vm::opcode::eq:  msg = "=="; break;
                    case vm::opcode::neq: msg = "!="; break;
                    case vm::opcode::lt:  msg = "<"; break;
                    case vm::opcode::lteq: msg = "<="; break;
                    case vm::opcode::gt:  msg = ">"; break;
                    case vm::opcode::gteq: msg = ">="; break;
                    case vm::opcode::shl: msg = "<<"; break;
                    case vm::opcode::shr: msg = ">>"; break;
                    }

                    oop_handle args;
                    oop_handle ctx = method_call_context(xa, msg, activeContext, 1, args);
                    args.get_obj()->set_field(1, xb);
                    set_context(ctx);
                    // oop_handle method_call_context (
                    //     const oop_handle& recv, const char* msg, 
                    //     const oop_handle& caller, std::size_t n_args, 
                    //     oop_handle& args)


                }
            }   break;

            default:
                Print1("invalid opcode " << (int)op << " " << vm::opcode_to_str(op) << " at " << ip);
                save_context();
                return error;

            }

        }

        save_context();

        return OK;

        #undef _push
        #undef _pop
        #undef _top
        #undef _print_stack
    }

    int run()
    {
        return_status r;
    cont:
        r = tick();
        if(r == OK)
            goto cont;
        return r;
    }

};

PMC VM::pmc;





}
